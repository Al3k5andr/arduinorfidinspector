#include <ESP8266WiFi.h>

const char* ssid = "SSID";
const char* password = "PASSWORD";
const char* host = "SERVER_IP";

WiFiClient client;
void setup()
{
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(50);
  }

  const int port = 8888;
  if (!client.connect(host, port))
  {
    Serial.println("connection failed");
    return;
  }
  else
  {
    delay(100);
    client.print(WiFi.config(ip) + "-ArduinoESP");
    delay(500);
  }
}

void loop()
{
  if (Serial.available()) {
    String Var = "";
    while (!Serial.available()) delay(20);
    delay(20);
    while (Serial.available())
      Var = Var + (char)(Serial.read());
    String msg = String(Var);
    if (msg != "") {
      client.print(msg);
    }
    String line = client.readString();


  }
}
